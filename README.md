# sofomo_task

The aim of this task is to build an API (backed by any kind of database) that requires JWT authorization. The application should be able to store geolocation data in the database, based on IP address or URL - you can use https://ipstack.com/ to get geolocation data (you can obtain free API KEY here -> https://ipstack.com/signup/free). The API should be able to add, delete or provide geolocation data on the base of ip address or URL. 


## configuration

see `env.example` and create `.env`

pay close attention to *SECRET_KEY*, *IPSTACK_API_KEY* and *ALLOWED_HOSTS*

```
cp env.example .env
```
set up according to your needs, but *IPSTACK_API_KEY* and *ALLOWED_HOSTS* are required.

Add Your server IP to ALLOWED_HOSTS like so:
```
ALLOWED_HOSTS=127.0.0.1,localhost
```


```
docker-compose build
docker-compose up

```

run db migrations:
```
docker-compose run backend python manage.py migrate
```

create super user:
```
docker-compose run backend python manage.py createsuperuser
```

start service:
```
docker-compose up
```

API server will run on `127.0.0.1:8000`

Visit `127.0.0.1:8000/admin/` to visit *Admin panel* where you can manage your data in GUI. You can login with user account you created before.

Optional: You can change `docker-compose.yml` accordign to your setting

## quick start

login with your super user credentials
```
curl \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{"username": "davidattenborough", "password": "boatymcboatface"}' \
  http://localhost:8000/api/token/

```
you will receive token:
```
{
  "refresh": REFRESH_TOKEN,
  "access": ACCESS-TOKEN
}
```

use `access token` to include in your request HEADER `-H "Authorization: Bearer ACCESS-TOKEN"`

optionaly: use 'refresh token` to obtain another access token
```
curl \
  -X POST \
  -H "Content-Type: application/json" \
  -d '{"refresh":REFRESH_TOKEN}' \
  http://localhost:8000/api/token/refresh/

```

Send post request to API server on POST `/api/location/` endpoint, to request for *ipstack* scan 
```
curl \
  -X POST \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer ACCESS-TOKEN" \
  http://localhost:8000/api/location/
```
results
```
{
  "id":1,
  "created_at":"2021-12-09T15:55:37.429333Z",
  "ip_stack":"{
    'ip': 'your ip', 
    'type': 'ipv4'
    ...
  }
}

```

## Test

test with pytest:
```
docker-compose run backend pytest .
```

## Notes

As super user you can create users

Only super users or staff users can see all location entries under GET /location/ endpoint.

Location `PUT` is not allowed. You cannot update data.

Location `DELETE` method is allowed only for super-users.

send POST with url_or_ip to pass to ipstack. leave data empty `{}` to capture your clients ip address.

Visit `127.0.0.1:8000/admin/` to visit *Admin panel*

Be careful, as *Admin panel* will allow you to modify/delete any data.

## API documentation

Run Api docker
```
docker-compose up
```


go to `http://127.0.0.1:8000/doc/` to see api documentation.
