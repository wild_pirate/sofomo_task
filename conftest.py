import pytest
from rest_framework.test import APIRequestFactory
from pytest_factoryboy import register
from users.tests.factories import UserFactory
from location.tests.factories import LocationFactory

register(UserFactory)
register(LocationFactory)


@pytest.fixture
def test_api():
    return APIRequestFactory()
