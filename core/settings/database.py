from shared.env import env

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": env("DB_NAME"),
        "USER": env("DB_USER"),
        "PASSWORD": env("DB_PASSWD"),
        "HOST": env("DB_HOST"),
        "PORT": env("DB_PORT"),
    }
}
