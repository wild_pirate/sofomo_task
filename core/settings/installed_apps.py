INSTALLED_APPS = [
    "location",
    "drf_yasg",
    "rest_framework_swagger",
    "users",
    "rest_framework_simplejwt",
    "rest_framework",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]
