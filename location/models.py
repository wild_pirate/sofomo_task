from django.db import models
from django.contrib.auth.models import User


class Location(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, related_name="locations", null=True, blank=True, on_delete=models.CASCADE)
    ip_stack = models.TextField(null=True, blank=True)
    url_or_ip = models.CharField(max_length=255)
