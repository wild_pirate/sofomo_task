from location.models import Location
import factory
from faker import Faker


fake = Faker()


class LocationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Location

    url_or_ip = 'https://cnn.com/'
    ip_stack = "{}"
