import json
from location.views import LocationViewSet
import pytest
from rest_framework_simplejwt.tokens import RefreshToken


@pytest.mark.django_db
class TestLocationApi:

    viewset = LocationViewSet
    endpoint = "/api/location/"
    password = "pass"
    auth_user = None
    token = None
    test_user = None

    regular_user = None
    regular_user_token = None
    location = None


    def gen_auth_superuser(self, test_api, user_factory):
        auth_user = user_factory.create()
        auth_user.set_password(self.password)
        auth_user.is_superuser = True
        auth_user.is_staff = True
        auth_user.save()
        self.auth_user = auth_user
        self.token = RefreshToken.for_user(self.auth_user)

    def gen_auth_user(self, test_api, user_factory):
        regular_user = user_factory.create(username="regular_auth_user")
        regular_user.set_password(self.password)
        regular_user.save()
        self.regular_user = regular_user
        self.regular_user_token = RefreshToken.for_user(self.regular_user)

    def auth(self, request, regular=False):
        token = self.token.access_token
        if regular:
            token = self.regular_user_token.access_token

        request.META["HTTP_AUTHORIZATION"] = f"Bearer {token}"
        return request

    def post_location(self, test_api):

        data = {"url_or_ip": "https://cnn.com"}
        request = test_api.post(self.endpoint, data)
        request = self.auth(request)

        response = self.viewset.as_view({"post": "create"})(request)
        _r = response.data.get("ip_stack", "{}").replace("'", "\"").replace("True", "true").replace("False", "false")
        response_data = json.loads(_r)
        region_name = response_data.get("region_name")
        assert response.status_code == 201
        assert region_name == "California"

    def update_location_as_regular(self, test_api):
        location = self.location
        update_location_data = {
            "id": location.id,
            "ip_stack": "{}",
        }

        url = f"{self.endpoint}{location.id}/"
        request = test_api.put(url, update_location_data)
        request = self.auth(request, regular=True)

        response = self.viewset.as_view({"put": "update"})(
            request, pk=location.id
        )
        assert response.status_code == 405

    def update_location(self, test_api):
        location = self.location
        update_location_data = {
            "id": location.id,
            "ip_stack": "{}",
        }

        url = f"{self.endpoint}{location.id}/"
        request = test_api.put(url, update_location_data)
        request = self.auth(request, regular=False)

        response = self.viewset.as_view({"put": "update"})(
            request, pk=location.id
        )
        assert response.status_code == 405

    def delete_location_as_regular(self, test_api):
        location = self.location
        url = f"{self.endpoint}{location.id}/"
        request = test_api.delete(url)
        request = self.auth(request, regular=True)

        response = self.viewset.as_view({"delete": "destroy"})(
            request, pk=location.id
        )
        assert response.status_code == 403

    def delete_location(self, test_api):
        location = self.location
        url = f"{self.endpoint}{location.id}/"
        request = test_api.delete(url)
        request = self.auth(request)

        response = self.viewset.as_view({"delete": "destroy"})(
            request, pk=location.id
        )

        assert response.status_code == 204

    def test_location_viewset(self, test_api, user_factory, location_factory):
        self.gen_auth_superuser(test_api, user_factory)
        self.gen_auth_user(test_api, user_factory)
        self.location = location_factory.create()

        self.post_location(test_api)
        self.update_location_as_regular(test_api)
        self.update_location(test_api)
        self.delete_location_as_regular(test_api)
        self.delete_location(test_api)
