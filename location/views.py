from django.conf import settings
from location.models import Location
from location.serializers import LocationSerializer
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
import requests


class LocationViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    http_method_names = ("get", "post", "delete", )

    def get_queryset(self):
        # if user is super user or staff, then can get all queryset
        user = self.request.user
        if user.is_staff or user.is_superuser:
            return Location.objects.all()
        return Location.objects.filter(user=user)

    def get_client_ip(self):
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = self.request.META.get('REMOTE_ADDR')
        return ip

    def api_ipstack(self, url_or_ip=None):
        if url_or_ip is None:
            url_or_ip = self.get_client_ip()
        url = f"http://api.ipstack.com/{url_or_ip}?access_key={settings.IPSTACK_API_KEY}"
        ipstack_response = requests.get(url)
        return ipstack_response.json()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        url_or_ip = request.data.get("url_or_ip")
        if isinstance(url_or_ip, str):
            url_or_ip = url_or_ip.replace("http://", "").replace("https://", "")

        instance = Location.objects.get(id=serializer.data["id"])
        instance.ip_stack = self.api_ipstack(url_or_ip)
        instance.url_or_ip = url_or_ip
        instance.user = self.request.user
        instance.save()

        serialized_data = LocationSerializer(instance).data
        return Response(serialized_data, status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        is_super_user = self.request.user.is_superuser
        if not is_super_user:
            return Response(status=403)

        return super().destroy(request, *args, **kwargs)
