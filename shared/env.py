import environ
import os
from pathlib import Path

_dir = Path(__file__).resolve().parent.parent

env = environ.Env(DEBUG=(bool, False))
environ.Env.read_env(os.path.join(_dir, ".env"))
