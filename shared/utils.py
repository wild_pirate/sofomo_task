from shared.env import env


def get_env_variable_as_int(variable):
    try:
        return int(env(variable))
    except (NameError, ValueError) as e:
        raise EnvironmentError(
            f"\n\nsee message:"
            f"\n\n\tEnvironmentError for {variable} environment variable:"
            f"\n\tInvalid value {e}"
            f"\n\nexit."
        )
