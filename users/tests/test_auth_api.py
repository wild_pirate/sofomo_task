import pytest
from rest_framework_simplejwt.views import TokenObtainPairView


@pytest.mark.django_db
class TestAuthApi:

    password = "pass"

    def login(self, test_api, user_factory):
        test_user = user_factory.create()
        test_user.set_password(self.password)
        test_user.save()

        view = TokenObtainPairView.as_view()
        auth_data = {"username": test_user.username, "password": self.password}
        request = test_api.post("/api/token/", auth_data)
        response = view(request)
        return response, test_user

    def test_user_login(self, test_api, user_factory):
        response, test_user = self.login(test_api, user_factory)
        assert "access" in response.data
        assert "refresh" in response.data
