from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import pytest
from rest_framework_simplejwt.tokens import RefreshToken
from users.views import UserViewSet


@pytest.mark.django_db
class TestUserApi:

    viewset = UserViewSet
    endpoint = "/api/users/"
    password = "pass"
    auth_user = None
    token = None
    test_user = None

    regular_user = None
    regular_user_token = None

    def gen_auth_superuser(self, test_api, user_factory):
        auth_user = user_factory.create()
        auth_user.set_password(self.password)
        auth_user.is_superuser = True
        auth_user.is_staff = True
        auth_user.save()
        self.auth_user = auth_user
        self.token = RefreshToken.for_user(self.auth_user)

    def gen_auth_user(self, test_api, user_factory):
        regular_user = user_factory.create(username="regular_auth_user")
        regular_user.set_password(self.password)
        regular_user.save()
        self.regular_user = regular_user
        self.regular_user_token = RefreshToken.for_user(self.regular_user)

    def auth(self, request, regular=False):
        token = self.token.access_token
        if regular:
            token = self.regular_user_token.access_token

        request.META["HTTP_AUTHORIZATION"] = f"Bearer {token}"
        return request

    def post_user(self, test_api):

        new_user_data = {"username": "created_test_user"}
        request = test_api.post(self.endpoint, new_user_data)
        request = self.auth(request)

        response = self.viewset.as_view({"post": "create"})(request)

        self.test_user = User.objects.get(id=response.data["id"])

        assert response.status_code == 201
        assert "id" in response.data
        assert "username" in response.data
        assert "password" in response.data

    def update_user_as_regular(self, test_api):
        updated_first_name = "updated_first_name"
        update_user_data = {
            "id": self.test_user.id,
            "username": self.test_user.username,
            "first_name": updated_first_name,
        }

        url = f"{self.endpoint}{self.test_user.id}/"
        request = test_api.put(url, update_user_data)
        request = self.auth(request, regular=True)

        response = self.viewset.as_view({"put": "update"})(
            request, pk=self.test_user.id
        )
        assert response.status_code == 403

    def update_user(self, test_api):
        updated_first_name = "updated_first_name"
        update_user_data = {
            "id": self.test_user.id,
            "username": self.test_user.username,
            "first_name": updated_first_name,
        }

        url = f"{self.endpoint}{self.test_user.id}/"
        request = test_api.put(url, update_user_data)
        request = self.auth(request)

        response = self.viewset.as_view({"put": "update"})(
            request, pk=self.test_user.id
        )

        u = User.objects.get(id=response.data["id"])
        self.test_user = u

        assert response.status_code == 200
        assert "id" in response.data
        assert "username" in response.data
        assert response.data["first_name"] == updated_first_name

    def delete_user_as_regular(self, test_api):

        url = f"{self.endpoint}{self.test_user.id}/"
        request = test_api.delete(url)
        request = self.auth(request, regular=True)

        response = self.viewset.as_view({"delete": "destroy"})(
            request, pk=self.test_user.id
        )
        assert response.status_code == 403

    def delete_user(self, test_api):

        url = f"{self.endpoint}{self.test_user.id}/"
        request = test_api.delete(url)
        request = self.auth(request)

        response = self.viewset.as_view({"delete": "destroy"})(
            request, pk=self.test_user.id
        )

        try:
            User.objects.get(id=self.test_user.id)
        except ObjectDoesNotExist:
            self.test_user = None

        assert response.status_code == 204
        assert self.test_user is None

    def test_user_viewset(self, test_api, user_factory):
        self.gen_auth_superuser(test_api, user_factory)
        self.gen_auth_user(test_api, user_factory)

        self.post_user(test_api)
        self.update_user_as_regular(test_api)
        self.update_user(test_api)
        self.update_user_as_regular(test_api)
        self.delete_user(test_api)
