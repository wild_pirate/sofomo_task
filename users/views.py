from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework import status
from rest_framework.response import Response
from users.serializers import UserSerializer
import uuid


class UserViewSet(ModelViewSet):
    permission_classes = (
        IsAuthenticated,
        IsAdminUser,
    )
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @staticmethod
    def set_random_password(serializer):
        password = uuid.uuid4().__str__().replace("-", "")

        user_id = serializer.data.get("id")
        if user_id is None:
            return (
                None,
                None,
                "[UserViewSet:set_random_password] Error parsing user id.",
            )

        try:
            user = User.objects.get(id=user_id)
        except ObjectDoesNotExist:
            return (
                None,
                None,
                "[UserViewSet:set_random_password] Error retrieving new user.",
            )

        user.set_password(password)

        return user, password, "created"

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        user, password, msg = self.set_random_password(serializer)
        user.is_staff = False
        user.save()

        if password is None:
            return Response({"msg": msg, status: status.HTTP_400_BAD_REQUEST})

        user_serialized = UserSerializer(user).data

        data = {**user_serialized, "password": password}
        return Response(data, status=status.HTTP_201_CREATED)
